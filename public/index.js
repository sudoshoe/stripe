const http = require('http')
const port = 80

const requestHandler = (request, response) => {
    let ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress || request.socket.remoteAddress || (request.connection.socket ? request.connection.socket.remoteAddress : null);
    cleanedIP = ip.split(':').slice(-1);

    let headers = JSON.stringify((request.headers), null, 4);

    response.end('IP Address: ' + (cleanedIP) + '\n\nHeaders: ' + headers)
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
})